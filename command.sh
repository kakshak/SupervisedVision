cd /opt/repo/code1

python3 classifier_baseline.py \
	--classes 16 \
	--workers 90 \
	--log_polar False \
	--salience False \
	--salience_points 4 \
	--training_aug 'random' \
	--dataset_path /data/identities_8 \
	--out_directory /data/output \
       	--experiment_name 1_id_8

